# Deviget Challenge Kotlin
## Reddit client that shows the top 50 entries from Reddit - www.reddit.com/top 

### Completed
-Pagination
-ApiRest with Coroutines 
-MVVM pattern using ViewModel and LiveData

### TODO
-Animation
-Unit Test
-Delete item (the data is loaded through inmutable data source)
-Saved image using tablet screen