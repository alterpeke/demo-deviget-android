package com.example.demodeviget.ui.main

import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.demodeviget.api.response.Data
import com.example.demodeviget.repository.EntryDataSource

class DemoDevigetViewModel() :
    ViewModel() {

    private var entriesPagedLiveData: LiveData<PagedList<Data>>
    var mutableEntriesPagedLiveData = MutableLiveData<PagedList<Data>>()
    val mediator = MediatorLiveData<Unit>()

    init {
        val config = PagedList.Config.Builder()
            .setInitialLoadSizeHint(10)
            .setPageSize(20)
            .setEnablePlaceholders(false)
            .build()
        entriesPagedLiveData = initializedPagedListBuilder(config).build()
        mediator.addSource(entriesPagedLiveData) { mutableEntriesPagedLiveData.value = it }
    }

    fun getPosts(): MutableLiveData<PagedList<Data>> = mutableEntriesPagedLiveData

    private fun initializedPagedListBuilder(config: PagedList.Config):
            LivePagedListBuilder<String, Data> {

        val dataSourceFactory = object : DataSource.Factory<String, Data>() {
            override fun create(): DataSource<String, Data> {
                return EntryDataSource(
                    viewModelScope
                )
            }
        }
        return LivePagedListBuilder(dataSourceFactory, config)
    }


}