package com.example.demodeviget.ui.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.demodeviget.R
import com.example.demodeviget.api.response.Data
import com.example.demodeviget.model.Entry
import com.example.demodeviget.ui.detail.EntryDetailActivity
import com.example.demodeviget.ui.detail.EntryDetailFragment
import com.example.demodeviget.ui.main.DemoDevigetActivity
import com.example.demodeviget.ui.paging.DiffUtilCallBack
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_entry.view.*
import java.util.concurrent.TimeUnit

class EntriesRecyclerViewAdapter(
    private val parent: DemoDevigetActivity,
    private val twoPane: Boolean
) :
    PagedListAdapter<Data, EntriesRecyclerViewAdapter.ViewHolder>(DiffUtilCallBack()) {

    private val onClickListener: View.OnClickListener
    private lateinit var buttonOnClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener {
            val itemData = it.tag as Data
            val item = itemData.let { it ->
                Entry(
                    it.id, it.clicked, it.author, it.thumbnail, it.created, it.url, it.title,
                    it.numComments
                )
            }
            if (twoPane) {
                val fragment = EntryDetailFragment()
                    .apply {
                        arguments = Bundle().apply {
                            putParcelable(EntryDetailFragment.DETAIL_ENTRY, item)
                        }
                    }
                parent.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
            } else {
                val intent = Intent(it.context, EntryDetailActivity::class.java).apply {
                    putExtra(EntryDetailFragment.DETAIL_ENTRY, item)
                }
                it.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_entry, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: EntriesRecyclerViewAdapter.ViewHolder, position: Int) {
        val item = getItem(position)!!
        holder.tvAuthor.text = item.author
        holder.tvTitle.text = item.title

        val res = parent.resources
        holder.tvComments.text =
            res.getQuantityString(R.plurals.comments_number, item.numComments, item.numComments)
        val hoursAgo =
            TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis() - item.created).toInt()
        holder.tvTimer.text = res.getQuantityString(R.plurals.entry_info, hoursAgo, hoursAgo)

        if (!item.thumbnail.isBlank()) {
            Picasso.get().load(item.thumbnail).into(holder.ivImage)
        } else {
            holder.ivImage.visibility = View.GONE
        }

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }

        buttonOnClickListener = View.OnClickListener {
            //TODO do delete item
        }

        with(holder.btDismiss) {
            setOnClickListener(buttonOnClickListener)
        }
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvAuthor: TextView = view.tv_author
        val tvTimer: TextView = view.tv_timer
        val ivImage: ImageView = view.iv_image
        val tvTitle: TextView = view.tv_title
        val btDismiss: Button = view.bt_dismiss
        val tvComments: TextView = view.tv_comments
    }

}