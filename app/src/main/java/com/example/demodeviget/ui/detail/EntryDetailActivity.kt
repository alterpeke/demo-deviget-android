package com.example.demodeviget.ui.detail

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import com.example.demodeviget.R
import com.example.demodeviget.model.Entry
import com.squareup.picasso.Picasso
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*

class EntryDetailActivity : AppCompatActivity() {

    private var image: String? = null
    private var myBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            val fragment = EntryDetailFragment()
                .apply {
                    arguments = Bundle().apply {
                        putParcelable(
                            EntryDetailFragment.DETAIL_ENTRY,
                            intent.getParcelableExtra(EntryDetailFragment.DETAIL_ENTRY)
                        )
                    }
                }
            val entry = intent.getParcelableExtra<Entry>(EntryDetailFragment.DETAIL_ENTRY)
            image = entry?.thumbnail
            supportFragmentManager.beginTransaction()
                .add(R.id.item_detail_container, fragment)
                .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                if (image.isNullOrBlank()) {
                    toast("Image not found")
                } else {
                    DownloadImage()
                }
            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            200 -> {
                bitmapToFile(myBitmap!!)
            }
            else -> toast("Denied permission to download file")
        }
    }

    private fun DownloadImage() {
        Picasso.get().load(image).into(object : com.squareup.picasso.Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                myBitmap = bitmap
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                toast("Processing...")
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                toast("Load image to download process has failed")
                return
            }
        })
        setupPermissions(this)
    }

    // Method to save an bitmap to a file
    private fun bitmapToFile(bitmap: Bitmap): Uri {
        // Get the context wrapper
        val wrapper = ContextWrapper(applicationContext)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
        file = File(file, "${UUID.randomUUID()}.png")

        try {
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            stream.flush()
            stream.close()
            toast("File downloaded in ${file.absolutePath}")
        } catch (e: IOException) {
            e.printStackTrace()
            toast("Error ond file downloading")
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }

    private fun setupPermissions(activity: Activity) {

        val permission = ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                activity,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 200
            )
        } else {
            bitmapToFile(myBitmap!!)
        }

    }
}

// Extension function to show toast message
fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

