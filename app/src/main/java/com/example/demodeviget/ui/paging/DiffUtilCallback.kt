package com.example.demodeviget.ui.paging

import androidx.recyclerview.widget.DiffUtil
import com.example.demodeviget.api.response.Data

class DiffUtilCallBack : DiffUtil.ItemCallback<Data>() {
    override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
        return oldItem == newItem
    }

}