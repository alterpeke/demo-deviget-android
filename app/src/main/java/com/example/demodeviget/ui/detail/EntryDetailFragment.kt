package com.example.demodeviget.ui.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.demodeviget.R
import com.example.demodeviget.model.Entry
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail.*

class EntryDetailFragment : Fragment(R.layout.fragment_detail) {

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val DETAIL_ENTRY = "DETAIL_ENTRY"
    }

    private var entry: Entry? = null
    var image: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            if (it.containsKey(DETAIL_ENTRY)) {
                entry = it.getParcelable(DETAIL_ENTRY)
                tv_author_label.text = entry?.author ?: " "
                if (!entry?.thumbnail.isNullOrBlank()) {
                    Picasso.get().load(entry?.thumbnail).into(iv_image_label)
                    image = entry?.thumbnail
                } else {
                    iv_image_label.visibility = View.GONE
                }
                tv_title_label.text = entry?.title ?: " "
            }
        }

    }


}