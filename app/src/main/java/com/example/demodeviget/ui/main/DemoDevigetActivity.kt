package com.example.demodeviget.ui.main

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.demodeviget.R
import com.example.demodeviget.ui.adapter.EntriesRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DemoDevigetActivity : AppCompatActivity() {

    private val demoDevigetViewModel: DemoDevigetViewModel by viewModel()
    private lateinit var entriesRecyclerViewAdapter: EntriesRecyclerViewAdapter
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (item_detail_container != null) {
            twoPane = true
        }

        entriesRecyclerViewAdapter = EntriesRecyclerViewAdapter(this, twoPane)

        progress_bar.visibility = View.VISIBLE
        demoDevigetViewModel.mediator.observe(this, Observer { })

        demoDevigetViewModel.getPosts().observe(this, Observer {
            progress_bar.visibility = View.GONE
            Log.e("DemoDevigetActivity", "Get ViewModel Paged Entries")
            entriesRecyclerViewAdapter.submitList(it)
        })

        rv_entries.adapter = entriesRecyclerViewAdapter


    }
}