package com.example.demodeviget.api.response

import com.google.gson.annotations.SerializedName

data class Children (
    @SerializedName("kind") val kind : String,
    @SerializedName("data") val data : Data
)
