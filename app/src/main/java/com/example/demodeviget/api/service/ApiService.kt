package com.example.demodeviget.api.service

import com.example.demodeviget.api.response.ResponseBase
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("top.json")
    suspend fun fetchPosts(
        @Query("limit") loadSize: Int = 20,
        @Query("after") after: String? = null,
        @Query("before") before: String? = null
    ): Response<ResponseBase>
}