package com.example.demodeviget.api.response

import com.google.gson.annotations.SerializedName

data class Data (

    @SerializedName("id") val id : String,
    @SerializedName("clicked") val clicked : Boolean,
    @SerializedName("author") val author : String,
    @SerializedName("thumbnail") val thumbnail : String,
    @SerializedName("created") val created : Int,
    @SerializedName("url") val url : String,
    @SerializedName("title") val title : String,
    @SerializedName("num_comments") val numComments : Int

)