package com.example.demodeviget.api.response

import com.google.gson.annotations.SerializedName

data class DataResponseBase (

    @SerializedName("modhash") val modhash : String,
    @SerializedName("children") val children : List<Children>,
    @SerializedName("after") val after : String,
    @SerializedName("before") val before : String

)