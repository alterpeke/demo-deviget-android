package com.example.demodeviget.di

import com.example.demodeviget.api.service.ApiClient
import com.example.demodeviget.ui.main.DemoDevigetViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { ApiClient() }
    viewModel { DemoDevigetViewModel() }

}

