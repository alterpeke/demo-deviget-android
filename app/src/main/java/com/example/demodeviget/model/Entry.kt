package com.example.demodeviget.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Entry(
    val id: String,
    val clicked: Boolean?,
    val author: String?,
    val thumbnail: String?,
    val created: Int?,
    val url: String?,
    val title: String?,
    val numComments: Int?
) : Parcelable